cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-customurlscheme.LaunchMyApp",
      "file": "plugins/cordova-plugin-customurlscheme/www/ios/LaunchMyApp.js",
      "pluginId": "cordova-plugin-customurlscheme",
      "clobbers": [
        "window.plugins.launchmyapp"
      ]
    },
    {
      "id": "cordova-plugin-device.device",
      "file": "plugins/cordova-plugin-device/www/device.js",
      "pluginId": "cordova-plugin-device",
      "clobbers": [
        "device"
      ]
    },
    {
      "id": "cordova-plugin-secure-key-store.SecureKeyStore",
      "file": "plugins/cordova-plugin-secure-key-store/www/SecureKeyStore.js",
      "pluginId": "cordova-plugin-secure-key-store",
      "clobbers": [
        "cordova.plugins.SecureKeyStore"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-customurlscheme": "4.4.0",
    "cordova-plugin-device": "2.0.2",
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-secure-key-store": "1.5.4"
  };
});