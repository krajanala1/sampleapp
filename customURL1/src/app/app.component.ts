import { Component } from '@angular/core';
declare var cordova: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    // cordova.plugins.SecureKeyStore.get(function (res) {
    //   alert(res); // res - string retrieved
    // }, function (error) {
    //   alert(error);
    // }, "key");

    (window as any).handleOpenURL = (url: string) => {
      setTimeout(() => {
                 this.handleOpenUrl(url);
                 }, 0);
      };
      // check if app was opened by custom url scheme
      const lastUrl: string = (window as any).handleOpenURL_LastURL || "";
      if (lastUrl && lastUrl !== "") {
      delete (window as any).handleOpenURL_LastURL;
      this.handleOpenUrl(lastUrl);
      }

  }
  handleOpenUrl = function (url) {
    var name = url.split("?");
    alert("deeplinking Parameters: " + name[1]);
    var regex = new RegExp(name);
    var results = regex.exec(window.location.href);
    if (name[1] == "foo=bar")
        return "";
    else
        return results[1];
};
}
