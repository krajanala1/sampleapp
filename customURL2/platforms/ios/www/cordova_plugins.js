cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "cordova-plugin-device.device",
      "file": "plugins/cordova-plugin-device/www/device.js",
      "pluginId": "cordova-plugin-device",
      "clobbers": [
        "device"
      ]
    },
    {
      "id": "cordova-plugin-inappbrowser.inappbrowser",
      "file": "plugins/cordova-plugin-inappbrowser/www/inappbrowser.js",
      "pluginId": "cordova-plugin-inappbrowser",
      "clobbers": [
        "cordova.InAppBrowser.open",
        "window.open"
      ]
    },
    {
      "id": "cordova-plugin-app-launcher.Launcher",
      "file": "plugins/cordova-plugin-app-launcher/www/Launcher.js",
      "pluginId": "cordova-plugin-app-launcher",
      "clobbers": [
        "plugins.launcher"
      ]
    },
    {
      "id": "cordova-plugin-secure-key-store.SecureKeyStore",
      "file": "plugins/cordova-plugin-secure-key-store/www/SecureKeyStore.js",
      "pluginId": "cordova-plugin-secure-key-store",
      "clobbers": [
        "cordova.plugins.SecureKeyStore"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-device": "2.0.2",
    "cordova-plugin-inappbrowser": "3.1.0",
    "cordova-plugin-whitelist": "1.3.3",
    "cordova-plugin-app-launcher": "0.4.0",
    "cordova-plugin-queries-schemes": "0.1.1",
    "cordova-plugin-secure-key-store": "1.5.4"
  };
});